## Descrição

Backend da aplicação Instaluence, que é uma ferramenta de automação para o Instagram. Ele é construído com NestJS
[Nest](https://github.com/nestjs/nest) que é um framework Nodejs com TypeScript.

## Instalação

```bash
$ npm install
```

## Iniciando a aplicação

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Iniciando testes

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```