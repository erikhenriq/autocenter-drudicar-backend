export default {
    env: process.env.NODE_ENV ? process.env.NODE_ENV : 'development',
    port: process.env.EXPRESS_PORT ? process.env.EXPRESS_PORT : 3000,
    secret_key: process.env.SECRET_KEY ? process.env.SECRET_KEY : '',
    mail_config: {
        user: process.env.MAIL_USER ? process.env.MAIL_USER : '',
        pass: process.env.MAIL_PASS ? process.env.MAIL_PASS : '',
        service: process.env.MAIL_SERVICE ? process.env.MAIL_SERVICE : '',
    },
};
