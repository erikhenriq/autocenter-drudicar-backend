import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from './api/user/user.service';
import { UserController } from './api/user/user.controller';
import { UserModule } from './api/user/user.module';
import { ConfigModule, ConfigService } from 'nestjs-config';
import * as path from 'path';
import { HandlebarsAdapter, MailerModule } from '@nest-modules/mailer';
import { AuthController } from './api/user/auth/auth.controller';
import { AuthService } from './api/user/auth/auth.service';
import { AuthModule } from './api/user/auth/auth.module';

@Module({
  imports: [
    ConfigModule.load(path.resolve(__dirname, 'config', '**', '!(*.d).{ts,js}')),
    MailerModule.forRootAsync({
      useFactory: () => ({transport: 'smtps://drudicar.noreply@gmail.com:t*H28Zfs+*5+Lx&V=nbH@smtp.gmail.com',
      defaults: {
        from: '"Autocenter Drudicar" <drudicar.noreply@gmail.com>',
      },
      template: {
        dir: __dirname + '/assets/templates',
        adapter: new HandlebarsAdapter(), // or new PugAdapter()
        options: {
          strict: true,
        },
      },
    }),
  }),
  UserModule,
  AuthModule,
  TypeOrmModule.forRootAsync({
    useFactory: (config: ConfigService) => config.get('database'),
    inject: [ConfigService],
  }),
  ],
  controllers: [UserController, AuthController],
  providers: [UserService, AuthService],
})
export class AppModule {}
