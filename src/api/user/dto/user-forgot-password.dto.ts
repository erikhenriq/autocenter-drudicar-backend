import { IsNotEmpty, IsEmail, IsString  } from 'class-validator';

export class UserForgotPasswordDto {

  @IsNotEmpty()
  @IsEmail()
  @IsString()
  readonly st_email: string;
}
