export interface UserData {
    expiresIn: number;
    accessToken: string;
}
