import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { User } from './user.entity';
import { UserForgotPasswordDto } from './dto';
import * as randomatic from 'randomatic';
import { MailerService } from '@nest-modules/mailer';

@Injectable()
export class UserService {

  constructor(
    private readonly mailerService: MailerService,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  update(user: User): Promise<UpdateResult> {
    return this.userRepository.update({ in_id: user.in_id }, user);
  }

  async findById(in_id: number): Promise<User> {
    const user: User = await this.userRepository.findOne(in_id);

    if (!user) {
      throw new HttpException({ user: 'user not found' }, HttpStatus.NOT_FOUND);
    }

    return user;
  }

  async findByEmail(st_email: string): Promise<User> {
    return this.userRepository.findOne({ st_email });
  }

  async requestNewPassword(userForgotPasswordDto: UserForgotPasswordDto): Promise<User> {
    const user = await this.userRepository.findOne({select:
      ['st_email', 'st_reset_password_token', 'dt_reset_password_expire', 'bo_is_active', 'in_id'],
      where: { st_email: userForgotPasswordDto.st_email } });

    if (!user) {
      throw new HttpException({
        message: 'Usuário nunca foi registrado no sistema.',
        errors: [ 'Usuário não encontrado.'],
      }, HttpStatus.NOT_FOUND);
    }

    if (!user.bo_is_active) {
      throw new HttpException({
        message: 'A conta não está ativada, primeiro ative sua conta verificando seu e-mail de Boas-vindas.',
        error: ['Usuário não ativado'] }, HttpStatus.FORBIDDEN);
    }

    try {
        user.st_reset_password_token = randomatic('a0', 60);

        user.dt_reset_password_expire = new Date();

        const promises = [];

        promises.push(this
          .mailerService
          .sendMail({
            to: user.st_email,
            subject: 'Problemas para tentar fazer login?? Calma..',
            template: 'reset_password.dat',
            context: {
              email: user.st_email,
              reset_token: user.st_reset_password_token,
            },
          }));

        promises.push(this.userRepository.createQueryBuilder()
        .update(User)
        .set({ dt_reset_password_expire: user.dt_reset_password_expire, st_reset_password_token: user.st_reset_password_token })
        .where('in_id = :in_id', { in_id: user.in_id })
          .execute());

        await Promise.all(promises);
    } catch (err) {
      throw new HttpException({
        message: 'error occurred trying to send reset password to user',
        error: { user: 'could not send reset password, get in touch with support' } }, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return;
  }

  findOneWithPassword(email: string): Promise<User> {
    return this.userRepository
    .createQueryBuilder('row')
    .select('row.in_id')
     .addSelect('row.st_password')
      .addSelect('row.st_email')
      .addSelect('row.dt_reset_password_expire')
      .addSelect('row.bo_is_active')
    .where('row.st_email = :email', { email })
    .getOne();
  }
}
