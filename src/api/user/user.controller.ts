import { Controller, Get, UsePipes, ValidationPipe, Post, Body, HttpCode, HttpStatus, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.entity';

import {
  ApiUseTags,
} from '@nestjs/swagger';
import { UserForgotPasswordDto } from './dto';
import { AuthGuard } from '@nestjs/passport';

@ApiUseTags('user')
@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @UseGuards(AuthGuard())
    @Get()
    findAll(): Promise<User[]> {
      return this.userService.findAll();
    }

    @UsePipes(new ValidationPipe())
    @Post('request-password')
    @HttpCode(HttpStatus.OK)
    async requestPassword(@Body() userForgotPasswordDto: UserForgotPasswordDto): Promise<User> {
        return this.userService.requestNewPassword(userForgotPasswordDto);
    }

    // @UsePipes(new ValidationPipe())
    // @Post('reset-password')
    // @HttpCode(HttpStatus.OK)
    // async resetPassword(@Body() userResetPasswordDto: UserForgotPasswordDto): Promise<User> {
    //     return this.userService.requestNewPassword(userResetPasswordDto);
    // }
}
