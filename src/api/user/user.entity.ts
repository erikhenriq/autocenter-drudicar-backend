import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({name: 'tb_user'})
export class User {
  @PrimaryGeneratedColumn()
  in_id: number;

  @Column({ length: 100, nullable: false })
  st_email: string;

  @Column({ length: 60, select: false, nullable: false })
  st_password: string;

  @Column({ default: false })
  bo_is_active: boolean;

  @Column('timestamp')
  dt_created: Date;

  @Column('timestamp')
  dt_updated: Date;

  @Column('timestamp', { nullable: true })
  dt_last_session: Date;

  @Column('timestamp', { select: false, nullable: true })
  dt_reset_password_expire: Date;

  @Column({ length: 60, select: false, nullable: true })
  st_reset_password_token: string;
}
