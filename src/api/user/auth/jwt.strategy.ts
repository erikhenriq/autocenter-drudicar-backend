import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthService } from './auth.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { ConfigService } from 'nestjs-config';
import { User } from '../user.entity';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
    configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get('environment').secret_key,
    });
  }

  async validate(payload: JwtPayload) {
    const user: User = await this.authService.validateUser(payload);

    if (!user) {
      throw new UnauthorizedException({ message: 'Usuário nunca foi registrado no sistema.',
        errors: ['Usuário não encontrado.'],
      });
    }

    if (!user.bo_is_active) {
      throw new UnauthorizedException({message: 'Usuário precisa estar ativo.',
        errors: ['Usuário desativado.'],
      });
    }

    return user;
  }
}
