import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { User } from '../user.entity';
import * as bcrypt from 'bcrypt';
import { UserService } from '../user.service';
import { UserLoginDto } from './dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService, private readonly jwtService: JwtService) { }

  async createToken(userLoginDto: UserLoginDto) {
    try {
      const user: User = await this.userService.findOneWithPassword(userLoginDto.st_email);

      if (!user) {
        throw new HttpException({
          message: 'never registrated',
          error: { user: 'user not found' },
        }, HttpStatus.NOT_FOUND);
      }

      if (!user.bo_is_active) {
        throw new HttpException({
          message: 'user must first activate account, check your e-mail',
          error: { user: 'user not activated' },
        }, HttpStatus.NOT_FOUND);
      }

      const isSamePassword = await bcrypt.compareSync(userLoginDto.st_password, user.st_password);

      if (isSamePassword === true) {
        user.dt_last_session = new Date();
        await this.userService.update(user);

        const userPayload: JwtPayload = { st_email: user.st_email };

        const accessToken = this.jwtService.sign(userPayload);

        return {
          expiresIn: 3600,
          accessToken,
        };
      }

      throw new HttpException({ message: 'not matched username and password', error: { user: 'invalid password.' } }, HttpStatus.FORBIDDEN);
    } catch (err) {
      throw err;
    }
  }

  async validateUser(payload: JwtPayload): Promise<any> {
    return this.userService.findByEmail(payload.st_email);
  }
}
