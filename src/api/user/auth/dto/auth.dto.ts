import { IsNotEmpty, IsEmail, IsString, ValidationArguments  } from 'class-validator';

export class UserLoginDto {

  @IsNotEmpty({
    message: (args: ValidationArguments) => {
      return 'Email não pode estar vazio.';
    },
  })
  @IsEmail({}, {
    message: (args: ValidationArguments) => {
      return 'Email tem que ser válido.';
    },
  })
  @IsString()
  readonly st_email: string;

  @IsNotEmpty()
  @IsString()
  readonly st_password: string;
}
