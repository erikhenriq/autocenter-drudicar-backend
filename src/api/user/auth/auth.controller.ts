import { Controller, Body, UsePipes, ValidationPipe, Post } from '@nestjs/common';
import {  } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { UserData } from '../user.interface';
import { ApiUseTags } from '@nestjs/swagger';
import { UserLoginDto } from './dto';

@ApiUseTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UsePipes(new ValidationPipe())
  @Post()
  async createToken(@Body() loginUserDto: UserLoginDto): Promise<UserData> {
    return this.authService.createToken(loginUserDto);
  }
}
