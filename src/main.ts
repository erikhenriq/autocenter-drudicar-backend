import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as helmet from 'helmet';
import * as sentry from '@sentry/node';
import { ConfigService } from 'nestjs-config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.use(helmet());
  app.setGlobalPrefix('api');

  const swaggerOptions = new DocumentBuilder()
    .setTitle('Autocenter Drudicar Backend Docs')
    .setDescription('This is the oficial specification for Autocenter Drudicar Backend implementation')
    .setVersion('1.0')
    .setBasePath('api')
    .build();
  const document = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup('swagger', app, document);
  console.log(ConfigService.get('database'))
  if (ConfigService.get('environment').env === 'production') {
    sentry.init({ dsn: 'https://17d5c869a3b340cd9713b7c41f53723f@sentry.io/1448342' });
  }

  await app.listen(ConfigService.get('environment').port);
}

bootstrap();
